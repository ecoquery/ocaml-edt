(** Lexicographic order comparisons *)
let lex access cmp fn v1 v2 =
  let v = fn v1 v2 in
  if v != 0 then v else cmp (access v1) (access v2)

(* let lex_r access cmp fn v1 v2 =
   let v = cmp (access v1) (access v2) in
   if v != 0 then v else fn v1 v2 *)

let eq _ _ = 0
(* let rec gcd n m = if m = 0 then n else gcd m (n mod m) *)

type date = CalendarLib.Calendar.Date.t
type heure = CalendarLib.Calendar.Time.t

let date (annee : int) (mois : int) (jour : int) : date =
  CalendarLib.Calendar.Date.make annee mois jour

let heure (h : int) (m : int) : heure = CalendarLib.Calendar.Time.make h m 0

let week_start_of_date d : date =
  let open CalendarLib.Calendar.Date in
  let day = day_of_week d in
  let offset = 1 - int_of_day day in
  add d (Period.day offset)

let to_isostring (dt: CalendarLib.Calendar.t): string =
  CalendarLib.Printer.Calendar.sprint "%FT%T" dt

(* type 'a cell = { rowspan : int; colspan : int; content : 'a }
   type 'a line = { headers : string cell list; cells : 'a cell list }

   type 'a table = {
     columns_headers : string cell list list;
     lines : 'a line list;
   } *)

module type ResourcesT = sig
  type t

  val conflicts : t -> t -> bool
  val compare : t -> t -> int
  val title : t -> string
  val description : t -> string
end

module Make (R : ResourcesT) = struct
  module C = CalendarLib.Calendar
  module P = CalendarLib.Calendar.Period
  module Pr = CalendarLib.Printer.Calendar

  type creneau = { resources : R.t; start : C.t; duration : int }
  type creneaux = creneau list
  type week = date * creneaux

  let creneau r dst tst dur =
    { resources = r; start = C.create dst tst; duration = dur }

  let resources c = c.resources
  let starts c = c.start
  let duration c = c.duration
  let new_week y m d = (week_start_of_date @@ C.Date.make y m d, [])
  let empty_creneaux = []

  let compare =
    eq
    |> lex (fun c -> c.start) C.compare
    |> lex (fun c -> c.duration) ( - )
    |> lex (fun c -> c.resources) R.compare

  (** [inside h h' d] renvoie true si h est dans l'intervalle [h', h'+d] *)
  let inside (h : C.t) (h' : C.t) d =
    let h'' = C.add h' (P.minute d) in
    C.compare h' h <= 0 && C.compare h h'' <= 0

  let intersect c1 c2 =
    inside c1.start c2.start c2.duration || inside c2.start c1.start c1.duration

  let conflict c1 c2 = intersect c1 c2 && R.conflicts c1.resources c2.resources

  exception Conflit of (creneau * creneau)

  let rec supprime_conflits raise_exc c l =
    match l with
    | [] -> l
    | c' :: rest ->
        if conflict c c' then
          if raise_exc then raise (Conflit (c, c'))
          else supprime_conflits raise_exc c rest
        else
          let l_s = supprime_conflits raise_exc c rest in
          if l_s == rest then l else l_s

  let rec place ?(raise_exc : bool = false) (c : creneau) (l : creneaux) :
      creneaux =
    match l with
    | [] -> [ c ]
    | c' :: rest when conflict c c' -> place ~raise_exc c rest
    | c' :: rest when compare c' c <= 0 -> c' :: place ~raise_exc c rest
    | c' :: rest -> c :: c' :: supprime_conflits raise_exc c rest

  let groupby gv cmp cmp2 l =
    let l = List.map (fun c -> (gv c, c)) l in
    let cmp_p = eq |> lex fst cmp |> lex snd cmp2 in
    let l = List.sort cmp_p l in
    let rec aux = function
      | [] -> []
      | (v, c) :: rest -> (
          match aux rest with
          | [] -> [ (v, [ c ]) ]
          | (v', cs') :: rest' ->
              if v = v' then (v', c :: cs') :: rest'
              else (v, [ c ]) :: (v', cs') :: rest')
    in
    aux l

  let by_week l =
    groupby
      (fun c -> week_start_of_date (C.to_date c.start))
      C.Date.compare compare l

  let week_start : week -> date = fst
  let ends c = C.add c.start (P.minute c.duration)

  let to_fullcalendar (weeks : (date * creneau list) list) : Yojson.Basic.t =
    let eid = ref 0 in
    let creneau_to_event c =
      let id = !eid in
      eid := !eid + 1;
      `Assoc
        [
          ("id", `String ("evt" ^ string_of_int id));
          ("title", `String (R.title c.resources));
          ("start", `String (to_isostring c.start));
          ("end", `String (to_isostring (ends c)));
        ]
    in
    `List
      (List.map (fun (_, crl) -> List.map creneau_to_event crl) weeks
      |> List.flatten)

  let write_fullcalendar ?(prefix = "") ?(suffix = "") (weeks : week list)
      (filename : string) =
    let ch = open_out filename in
    output_string ch prefix;
    to_fullcalendar weeks |> Yojson.Basic.pretty_to_channel ch;
    output_string ch suffix;
    close_out_noerr ch

  let simple_page weeks filename =
    let tpl =
      {|
<!DOCTYPE html>
<html lang='fr-FR'>
  <head>
    <meta charset='utf-8' />
    <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.15/index.global.min.js'></script>
    <script>

      document.addEventListener('DOMContentLoaded', function() {
        var data = $$DATA$$;
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          initialView: 'timeGridWeek',
          events: data,
          locale: 'fr',
          aspectRatio: 2
        });
        calendar.render();
      });

    </script>
  </head>
  <body>
    <div id='calendar' style='width: 80%; padding: 20px;'></div>
  </body>
</html>
      |}
    in
    let output =
      Str.global_replace
        (Str.regexp_string "$$DATA$$")
        (to_fullcalendar weeks |> Yojson.Basic.to_string)
        tpl
    in
    let ch = open_out filename in
    output_string ch output;
    close_out_noerr ch
end
