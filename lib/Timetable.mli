type date
type heure

val date : int -> int -> int -> date
val heure : int -> int -> heure

module type ResourcesT = sig
  type t

  val conflicts : t -> t -> bool
  val compare : t -> t -> int
  val title : t -> string
  val description : t -> string
end

module Make (R : ResourcesT) : sig
  type creneau
  (** Type pour représenter un créneau associé à des ressources *)
    
  val creneau: R.t -> date -> heure -> int -> creneau
  (** Créée un nouveau créneau *)

  val resources : creneau -> R.t
  (** ressources associées à un créneau *)

  val starts : creneau -> CalendarLib.Calendar.t
  (** heure de début du créneau *)

  val duration : creneau -> int
  (** heure de fin du créneau *)

  type creneaux
  (** Représente un ensemble de créneaux *)

  type week
  (** Représente une semaine *)

  val new_week : int -> int -> int -> week
  (** [new_week y m d] crée une nouvelle semaine contenant le jour passé en
    argument sous la forme [y-m-d] (année, mois, jour)*)

  exception Conflit of (creneau * creneau)
  (** Conflit de créneaux *)

val empty_creneaux: creneaux
(** ensemble de créneaux vides *)

  val place : ?raise_exc:bool -> creneau -> creneaux -> creneaux
  (** [place b cr crl] ajoute le créneau [cr] à la liste des créneaux [crl] si
        ce créneau rentre en conflit avec un créneau dans [crl], alors si [b]
        vaut [true] une exception [Conflit] est levée. Si [b] vaut [false], alors
        le nouveau créneau prend la place des créneaux avec lesquels il est en
        conflit.
    *)

  val by_week : creneaux -> week list
  (** [by_week] prend un paquet de créneaux et construit une liste de semaines *)

  val week_start : week -> date
  (** date du premier jour de la semaine *)

  val to_fullcalendar : week list -> Yojson.Basic.t
  (** convertit un ensemble de semaines en structure json utilisable comme
    donnée pour le composant javascript FullCalendar *)

  val write_fullcalendar :
    ?prefix:string -> ?suffix:string -> week list -> string -> unit
  (** [write_fullcalendar weeks filename]
    convertit une liste de semaines [weeks] en json et l'écrit dans le fichier nommé [filename].
    [?prefix] est écrit avant le json dans le fichier
    [?suffix] est écrit après le json dans le fichier *)

  val simple_page : week list -> string -> unit
  (** [simple_page weeks filename] écrit une page HTML simple permettant de
    visualiser les événements compris dans weeks *)
end
