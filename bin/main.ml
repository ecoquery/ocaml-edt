(*
Copyright 2024 Emmanuel Coquery

This file is part of OCamlEDT.

OCamlEDT is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

OCamlEDT is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
OCamlEDT. If not, see <https://www.gnu.org/licenses/>.

*)
open Cmdliner

let run () = print_endline "Hello, World!"
let run_t = Term.(const run $ const ())
let cmd = Cmd.v (Cmd.info "ocaml_edt") run_t
let () = exit (Cmd.eval cmd)
