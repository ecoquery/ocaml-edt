# usage: make [target]
#
# default target is help
#
# targets:

default: help

#   test: runs unit tests
test:
	opam exec dune test

#   setup: installs a local switch as well as dependencies for the program
setup: _opam deps dev-deps

#   deps: install program dependencies
deps: _opam
	opam install --deps-only .

dev-deps: _opam
	opam install ocaml-lsp-server ocamlformat

_opam:
	opam switch create ./ ocaml-base-compiler.5.2.0

#   build: recompiles the application
build: _opam
	opam exec dune build

#   continuous build
watch: _opam
	opam exec -- dune build --watch

#   help: shows the possible targets
help:
	@sed -ne 's/^#\(.*\)$$/\1/p' Makefile

.PHONY: default setup deps dev-deps help test build
