(*
Copyright 2024 Emmanuel Coquery

This file is part of OCamlEDT.

OCamlEDT is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

OCamlEDT is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
OCamlEDT. If not, see <https://www.gnu.org/licenses/>.

*)
open OUnit2
open Ocaml_edt.Timetable

module StringResource = struct
  type t = string

  let conflicts _s1 _s2 = true
  let compare = String.compare
  let title s = s
  let description s = s
end

module TT = Make (StringResource)
open TT

let creneaux =
  empty_creneaux
  |> place (creneau "a" (date 2024 09 10) (heure 8 0) 90)
  |> place (creneau "b" (date 2024 09 10) (heure 14 0) 195)
  |> place (creneau "c" (date 2024 09 11) (heure 8 0) 90)

let test_write_html _ = simple_page (by_week creneaux) "test.html"

let suite =
  "Ocaml_edt test suite" >::: [ "test_write_html" >:: test_write_html ]

let () = run_test_tt_main suite
